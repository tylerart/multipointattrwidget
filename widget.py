from PySide2 import QtWidget, QtCore


class MultiWidgetUI(QtWidget.QDialog):

    def __init__(self):
        super(MultiWidgetUI, self).__init__()

    def create_ui(self):
        self.setWindowTitle('Widget')
        self.setWindowFlags(self.windowFlags() |
                            QtCore.Qt.WindowSystemMenuHint |
                            QtCore.Qt.WindowMinMaxButtonsHint)

