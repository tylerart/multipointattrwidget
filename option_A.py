from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from maya.app.general.mayaMixin import MayaQWidgetBaseMixin
import pymel.core as pm
from maya import cmds
from maya import OpenMayaUI as omui
import shiboken2 as shiboken


class Window(MayaQWidgetBaseMixin, QWidget):
    def __init__(self, parent=None, *args, **kwargs):
        super(Window, self).__init__(*args, **kwargs)
        # Destroy this widget when closed. Otherwise it will stay around
        ptr = omui.MQtUtil.mainWindow()
        mainWin = shiboken.wrapInstance(long(ptr), QWidget)
        self.setAttribute(Qt.WA_DeleteOnClose, True)
        self.setupUi()

        cmds.setParent('Form|mainLayout|ramp_layout')
        ctrl = """falloffCurveAttr -h 150 -at Flame:flameShape.opacity;"""
        pm.mel.eval(ctrl)
        self.show()

    def setupUi(self):
        self.setObjectName("Form")
        self.resize(400, 400)
        self.setWindowTitle("ramp test")
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName("mainLayout")

        self.w = QWidget(self)
        rampWidgetName = "rampWidget3"
        self.w.setObjectName(rampWidgetName)
        self.l = QVBoxLayout()
        self.l.setObjectName("ramp_layout")
        self.w.setLayout(self.l)
        self.w.resize(400, 400)

        self.verticalLayout.addWidget(self.w)
        self.setLayout(self.verticalLayout)



window = Window()

